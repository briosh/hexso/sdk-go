package hexso

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
)

const (
	version                      = "0.0.1"
	defaultApiURL                = "https://api.hexso.fr/v1"
	defaultMaxIdleConnsPerHost   = 20
	defaultTimeout               = time.Second * 30
	defaultTLSHandshakeTimeout   = time.Second * 5
	defaultResponseHeaderTimeout = time.Second * 30
	defaultForceAttemptHTTP2     = true
)

// NewClient makes a new Client capable of making API requests.
func NewClient(token string, opts ...ClientOption) (*Client, error) {
	var client = &Client{
		apiURL: defaultApiURL,
		token:  token,
		debug:  false,
		httpClient: &http.Client{
			Timeout: defaultTimeout,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					MinVersion: tls.VersionTLS12,
					MaxVersion: tls.VersionTLS13,
				},
				MaxIdleConnsPerHost:   defaultMaxIdleConnsPerHost,
				TLSHandshakeTimeout:   defaultTLSHandshakeTimeout,
				ResponseHeaderTimeout: defaultResponseHeaderTimeout,
				ForceAttemptHTTP2:     defaultForceAttemptHTTP2,
			},
		},
	}

	for _, opt := range opts {
		opt(client)
	}

	return client, nil
}

// Client is a client for interacting with API.
type Client struct {
	apiURL     string
	token      string
	debug      bool
	httpClient *http.Client
}

// Execute the query and unmarshal the response from the data field.
func (c Client) Execute(method, uri string, request, response interface{}) error {
	// Validate request
	if err := c.validate(request); err != nil {
		return err
	}

	// Execute request
	data, err := c.Request(method, uri, nil)
	if err != nil {
		return err
	}

	// Parse response
	if data != nil {
		if err = json.Unmarshal(data, &response); err != nil {
			return err
		}
	}

	return nil
}

// Request the query and return data.
func (c Client) Request(method string, uri string, body interface{}) ([]byte, error) {
	return c.RequestWithContext(context.Background(), method, uri, body)
}

// RequestWithContext the query and return data.
func (c Client) RequestWithContext(ctx context.Context, method string, uri string, body interface{}) ([]byte, error) {
	// Prepare request body
	var requestBody io.Reader
	if body != nil {
		switch v := body.(type) {
		case io.Reader:
			requestBody = v
		case []byte:
			requestBody = bytes.NewReader(v)
		default:
			var bodyBuffer = new(bytes.Buffer)

			if err := json.NewEncoder(bodyBuffer).Encode(v); err != nil {
				return nil, err
			}

			requestBody = bodyBuffer
		}
	}

	// Prepare request
	req, err := http.NewRequestWithContext(ctx, method, c.apiURL+uri, requestBody)
	if err != nil {
		return nil, err
	}

	// Set request headers
	req.Header.Set("User-Agent", "hexso-sdk-go/"+version)
	req.Header.Set("Content-Type", "application/json;charset=utf-8")
	if c.token != "" {
		req.Header.Set("X-Api-Token", c.token)
	}

	// Execute request
	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	// No content return empty
	if res.StatusCode == http.StatusNoContent {
		return make([]byte, 0), nil
	}

	// API Error
	if res.StatusCode < http.StatusOK || http.StatusMultipleChoices <= res.StatusCode {
		var apiError Error

		if err = json.NewDecoder(res.Body).Decode(&apiError); err != nil {
			return nil, err
		}

		return nil, apiError
	}

	// Read and return body response
	return ioutil.ReadAll(res.Body)
}

// validate a structure
func (c Client) validate(i interface{}) error {
	v := validator.New()

	return v.Struct(i)
}

// ClientOption are functions that are passed into NewClient to modify the behaviour of the Client.
type ClientOption func(*Client)

// WithAPIURL client option overrides the API URL of the Hexso API to the given URL.
func WithAPIURL(apiURL string) ClientOption {
	return func(client *Client) {
		if apiURL != "" {
			client.apiURL = apiURL
		}
	}
}

// Error API response
type Error struct {
	Status    int    `json:"status"`
	Message   string `json:"message"`
	RequestID string `json:"requestId"`
}

// Error conventional interface
func (e Error) Error() string {
	return fmt.Sprintf("%d : %s", e.Status, e.Message)
}

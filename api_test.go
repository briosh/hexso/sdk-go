package hexso

import (
	"net/http"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestClient_GetHealth(t *testing.T) {
	t.Run("Valid API", func(t *testing.T) {
		client, err := NewClient(os.Getenv("HEXSO_TOKEN"), WithAPIURL(os.Getenv("HEXSO_ENDPOINT")))
		if err != nil {
			t.Fatal(err)
		}
		res, err := client.GetHealth(HealthRequest{})
		assert.Equal(t, HealthResponse{StatusText: "UP", StatusCode: http.StatusOK}, *res)
		assert.NoError(t, err)
	})

	t.Run("Invalid API path", func(t *testing.T) {
		client, err := NewClient(os.Getenv("HEXSO_TOKEN"), WithAPIURL("http://127.0.0.1:3000"))
		if err != nil {
			t.Fatal(err)
		}
		res, err := client.GetHealth(HealthRequest{})
		assert.Nil(t, res)
		assert.IsType(t, Error{}, err)
		assert.Equal(t, err.(Error).Status, http.StatusNotFound)
		assert.Equal(t, err.(Error).Message, http.StatusText(http.StatusNotFound))
		assert.EqualError(t, err, "404 : Not Found")
	})

	t.Run("Invalid API server", func(t *testing.T) {
		client, err := NewClient(os.Getenv("HEXSO_TOKEN"), WithAPIURL("http://127.0.0.1:123456789"))
		if err != nil {
			t.Fatal(err)
		}
		res, err := client.GetHealth(HealthRequest{})
		assert.Nil(t, res)
		assert.Error(t, err)
	})
}

package hexso_test

import hexso "gitlab.com/briosh/hexso/sdk-go"

func Example_apiClient() {
	// Create a Hexso client
	client, err := hexso.NewClient("API_TOKEN")
	if err != nil {
		// handle error
	}

	// Start using the SDK
	_, _ = client.GetHealth(hexso.HealthRequest{})
}

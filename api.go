package hexso

import (
	"net/http"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Health
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
type (
	HealthRequest struct{}

	HealthResponse struct {
		StatusText string `json:"statusText"`
		StatusCode int    `json:"statusCode"`
	}
)

func (c Client) GetHealth(req HealthRequest) (*HealthResponse, error) {
	var res *HealthResponse

	if err := c.Execute(http.MethodGet, "/health", req, &res); err != nil {
		return nil, err
	}

	return res, nil
}
